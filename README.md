# Collection of various Sprinter software with source codes

When possible different versions of sources were commited with chronological order one after another
to see the difference between them. All files were converted from DOS to UNIX line ending (if it's
compilable in Linux) and from DOS (or Windows) encoding to UTF8 for Russian comments that enables
online browsing of that sources in readable form.

Our understanding is that most software included here are PUBLIC DOMAIN (if not specified differently).
If you are the author of any part of included sources and you are not agree with this consideration
then please contact us to resolve the issue.

For more info see Sprinter Unofficial http://sprinter.nedopc.org
