ClearScr:	ld bc,0x0756			;c = ESTEX_clearscr
		ld de,0
		ld hl,0x2050
		ld a,space
		rst 0x10
		ld de,0
		SYS ESTEX_setcursor
		ld de,0
		ld (coords),DE
		ret

;in:
;IX = buf addr
ScrBackup:	ld de,0				;XY 0,0
		ld hl,0x2050			;h,w: 32x80
		ex af,af
		in a,(mmu3)
		ex af,af
		SYS ESTEX_scrwincopy
		ret

;in:
;IX = buf addr
ScrRestore:	ld de,0				;XY 0,0
		ld hl,0x2050			;h,w: 32x80
		ex af,af
		in a,(mmu3)
		ex af,af
		SYS ESTEX_scrwinrest


;проверка, включён ли тестовый режим. если нет, то включает.
Check_Vmode:	SYS ESTEX_getvmode
			jr nc,.next0
		jp getvmodeErr
.next0:		ld (vmode),a			;current vmode
		ex af,af
		ld a,b
		ld (vmode.screen),a		;screen (0 or 1)
		ex af,af
		cp _VMODE._T80			;text 80x32 mode?
			call nz,.set_t80_mode	;if no, then set it
		ret



.set_t80_mode:	ld a,_VMODE._T80
		SYS ESTEX_setvmode
		jp c,setvmodeErr
		ret


getvmodeErr:	PrintChars errGetvmodeMsg
		ld b,-1
		jp quit0


setvmodeErr:	PrintChars errSetvmodeMsg
		ld b,-2
		jp quit0

vmode:		db 0
.screen:	db 0

errGetvmodeMsg:	db cr,lf
		db "ERROR: Failed to get video mode!",cr,lf
		db cr,lf,0

errSetvmodeMsg:	db cr,lf
		db "ERROR: Failed to set video mode!",cr,lf
		db cr,lf,0
		


PrintSym:	ex af,af
		ld de,(coords)
		ld a,(print_attr)
		ld b,a
		ex af,af
		SYS ESTEX_wrchar		;print char with attribute
		ret
;Input:
;DE = YX
GoToXY:		ld (coords),de
		ret
