;*******************************************************************************
;---===Timer V.2===---
;original timer.exe by Ivan Makarchenko aKa Imak (aKa Winglion) (C) 2002
;V.2 mod by Miroshnichenko Alexander aKa Sayman@SprinterTeam (C) 2021
;!free for use! but with original copyrights!
;*******************************************************************************

		DEVICE 	ZXSPECTRUM128

Start_addr = 0x8100
		include "..\..\include\estex_h.asm"
		include "..\..\include\head_short.inc"
		include "..\..\include\macro.s"

		include "constants.asm"


LoaderStart:	di
		jp main
;-------------------------------------------------------------------------------
;misc functions
init0:		ld a,(ix-3)
		SYS ESTEX_fclose

		in a,(mmu0)			;save values from all banks (mmu`s)
		ld (MMU._0),a
		in a,(mmu1)
		ld (MMU._1),a
		in a,(mmu2)
		ld (MMU._2),a
		in a,(mmu3)
		ld (MMU._3),a

		ld b,1				;get 1 page
		SYS ESTEX_getmem
		jp c,memErr			;if not available, then error

		ld (mem.hndl),a			;save page handle
		ld b,0				;if b=1, then BIOS return FF code (last blk)
		BIOS BIOS_get_mem_pg
		ld (mem.pg),a
		out (mmu3),a

		call Check_Vmode
;		call ScrBackup
		call ClearScr
		di
		call fastram_on
		call copy_to_fram
		call fastram_off

		call check_isa
		ld a,(use_isa0)
		or a
		jp z,.next_isa
		ld de,(ISA.SLOT0_RAM*256)+0x37
		call isa_on
		call copy_to_iram
		call isa_off
.next_isa:	ld a,(use_isa1)
		or a
		ret z
		ld de,(ISA.SLOT1_RAM*256)+0x37
		call isa_on
		call copy_to_iram
		call isa_off
		ret

turbo_on:	ld a,d_tbon
		out (sys_port_off),a
		ret

turbo_off:	ld a,d_tboff
		out (sys_port_off),a
		ret

fastram_on:	in a,(0xfb)
		ret

fastram_off:	in a,(0x7b)
		ret


check_isa:	ld de,(ISA.SLOT0_RAM*256)+0x37	;для совместимости со SprinterNet
		call isa_on
		ld a,0xaa
		ld (0xc800),a
		ld a,0x55
		ld (0xc801),a
		ld a,(0xc800)
		cp 0xaa
		jr nz,.next0
		ld (use_isa0),a
.next0:		ld de,(ISA.SLOT1_RAM*256)+0x37
		call isa_on
		ld a,0xaa
		ld (0xc800),a
		ld a,0x55
		ld (0xc801),a
		ld a,(0xc800)
		cp 0xaa
		ret nz
		ld (use_isa1),a
		ret

;-------------------------------------------------------------------------------
;If you want to interaction with ISA devices, you have to make following steps:
;1) send 10h value to port 1FFDh(system port);
;2) send control byte to port 0E2h(third memory window port);
;control byte:
;D7...should be 1
;D6...should be 1
;D5...should be 0
;D4...should be 1
;D3...should be 0
;D2...specify access mode (0 - ISA memory, 1 - ISA ports)
;D1...specify number of ISA slot
;D0...should be 0

;The read/write signals are forming from read/write signals memory range 0C000h-0FFFFh.
;And the address lines A13...A0 has taken from processor data-BUS.
;The other ISA-signals such as RESET, AEN, A19...A14 can be set in port 9FBDh. And default value is 00h.
;port 9FBDh:
;D7...RESET
;D6...AEN
;D5...A19
;D4...A18
;D3...A17
;D2...A16
;D1...A15
;D0...A14
;IN:
;D = SLOT (0 or 1) with access method (ram or ports)
;E = ISA RAM bank
isa_on:		ld bc,sc_port
		ld a,0x11
		out (c),a
		ld a,d
		out (mmu3),a
		ld bc,ISA.DIR
		out (c),e
		ret

isa_off:	ld bc,sc_port
		ld a,1
		out (c),a
		ld a,(mem.pg)
		out (mmu3),a
		ret


;Inputs:
;     HL is the numerator
;     C is the denominator
;Outputs:
;     A is the remainder
;     B is 0
;     C is not changed
;     DE is not changed
;     HL is the quotient
HL_Div_C:	ld b,16
		xor a
		add hl,hl
		rla
		cp c
		jr c,$+4
		inc l
		sub c
		djnz $-7
		ret


copy_to_fram:	ld hl,_fast_ram_test_start
		ld de,0x100
		ld bc,_fast_ram_test_size
		ldir
		ret

copy_to_iram:	ld hl,_isa_ram_test_start
		ld de,0xc800
		ld bc,_isa_ram_test_size
		ldir
		ret
;-------------------------------------------------------------------------------
		include "console.asm"
		include "printf.asm"
;-------------------------------------------------------------------------------

main:		call init0

		ld de,MsgBeginPtr
		call cPrint

		call main_test		;основной цикл теста
		jp c,TestErr


		ld b,0
		jp quit0
		



main_test:
;inc/dec
		ld de,TestMsg			;inc/dec печать тестируемой инфы (список)
		call cPrint			;цветная печать

		ld a,(use_isa0)			;test isa0 ram flag
		or a
		jr z,.next0			;no isa0 ram
		ld hl,msgIncDec_isa0
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint		

.next0:		ld a,(use_isa1)
		or a
		jr z,.next1
		ld hl,msgIncDec_isa1
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint
;push hl
.next1:		ld hl,msgPush
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint

		ld a,(use_isa0)
		or a
		jr z,.next2
		ld hl,msgPush_isa0
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint		

.next2:		ld a,(use_isa1)
		or a
		jr z,.next3
		ld hl,msgPush_isa1
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint
;pop hl
.next3:		ld hl,msgPop
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint

		ld a,(use_isa0)
		or a
		jr z,.next4
		ld hl,msgPop_isa0
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint		

.next4:		ld a,(use_isa1)
		or a
		jr z,.next5
		ld hl,msgPop_isa1
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint
;ld a,(hl)
.next5:		ld hl,msgLda
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint

		ld a,(use_isa0)
		or a
		jr z,.next6
		ld hl,msgLda_isa0
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint		

.next6:		ld a,(use_isa1)
		or a
		jr z,.next7
		ld hl,msgLda_isa1
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint
;ld (hl),a
.next7:		ld hl,msgLdhl
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint

		ld a,(use_isa0)
		or a
		jr z,.next8
		ld hl,msgLdhl_isa0
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint		

.next8:		ld a,(use_isa1)
		or a
		jr z,.next9
		ld hl,msgLdhl_isa1
		ld (TestMsg),hl
		ld de,TestMsg
		call cPrint

.next9:
		include "test_main.asm"
		
		





;-----
;тесты
		include "test_mainram.asm"
		include "test_fastram.asm"
		include "test_isaram.asm"



		
;--------------------------------------
;обработка ошибок
TestErr:	PrintChars TestErrMsg
		ld b,-2
		jp quit0

memErr:		PrintChars memErrMsg
		ld b,-1

quit0:		SYS ESTEX_exit
;--------------------------------------

MMU:
._0:		db 0
._1:		db 0
._2:		db 0
._3:		db 0


mem:
.hndl:		db 0
.pg:		db 0

use_isa0:	db 0
use_isa1:	db 0

sav_sp:		dw 0


MsgBeginPtr:	dw MsgBegin

;********************************
;inc/dec info
TestMsg:	dw msgIncDec

;inc/dec 3.5mhz result`s
result_tst_std:
		dw msgResult
.MHz:		dw 0
.KHz:		dw 0

;inc/dec in turbo result`s
result_tst_trb:
		dw msgResult
.MHz:		dw 0
.KHz:		dw 0

;********************************
;PUSH rr info
;PUSHrrPtr_pre:	dw msgPUSHrr_pre

;PUSH rr 3.5mhz result`s
;PUSHrrPtrRes_std:
;		dw msgPUSHrr_res
;.MHz:		dw 0
;.KHz:		dw 0

;PUSH rr in turbo result`s
;PUSHrrPtrRes_trb:
;		dw msgPUSHrr_res
;.MHz:		dw 0
;.KHz:		dw 0

;********************************



;--------------------------------------
;messages
memErrMsg:	db cr,lf
		db "ERROR: Memory allocation failed!",cr,lf
		db cr,lf,0

TestErrMsg:	db cr,lf
		db "ERROR: Test failed!",cr,lf
		db cr,lf,0



MsgBegin:	db "--------------------------------------------------------------------------------"
		db "+++ Test CPU and memory timing`s +++",cr,lf
		db "--------------------------------------------------------------------------------",0

;		    [INC/DEC HL]	[3.50 MHz/10.8  MHz]	in Main-RAM
;		    [INC/DEC HL]	[3.50 MHz/20.92 MHz]	in Fast-RAM
;		    [PUSH rr]		[3.50 MHz/10.8  MHz]	in Main-RAM
;		    [PUSH rr]		[3.50 MHz/20.92 MHz]	in Fast-RAM
;		    [POP rr]		[3.50 MHz/10.8  MHz]	in Main-RAM
;		    [POP rr]		[3.50 MHz/20.92 MHz]	in Fast-RAM
;		    [LD A,(HL)]		[3.50 MHz/10.8  MHz]	in Main-RAM
;		    [LD A,(HL)]		[3.50 MHz/20.92 MHz]	in Fast-RAM

msgIncDec:	db col_cmd, col_violet,"[INC/DEC HL]", col_cmd, col_white, "	[     MHz/      MHz]	in Main-RAM",cr,lf
		db col_cmd, col_violet,"[INC/DEC HL]", col_cmd, col_white, "	[     MHz/      MHz]	in Fast-RAM",cr,lf,0
msgIncDec_isa0:	db col_cmd, col_violet,"[INC/DEC HL]", col_cmd, col_white, "	[     MHz/      MHz]	in ISA1-RAM",cr,lf,0
msgIncDec_isa1:	db col_cmd, col_violet,"[INC/DEC HL]", col_cmd, col_white, "	[     MHz/      MHz]	in ISA2-RAM",cr,lf,0

msgPush:	db col_cmd, col_violet,"[PUSH HL]", col_cmd, col_white, "		[     MHz/      MHz]	in Main-RAM",cr,lf
		db col_cmd, col_violet,"[PUSH HL]", col_cmd, col_white, "		[     MHz/      MHz]	in Fast-RAM",cr,lf,0
msgPush_isa0:	db col_cmd, col_violet,"[PUSH HL]", col_cmd, col_white, "		[     MHz/      MHz]	in ISA1-RAM",cr,lf,0
msgPush_isa1:	db col_cmd, col_violet,"[PUSH HL]", col_cmd, col_white, "		[     MHz/      MHz]	in ISA2-RAM",cr,lf,0

msgPop:		db col_cmd, col_violet,"[POP HL]", col_cmd, col_white, "		[     MHz/      MHz]	in Main-RAM",cr,lf
		db col_cmd, col_violet,"[POP HL]", col_cmd, col_white, "		[     MHz/      MHz]	in Fast-RAM",cr,lf,0
msgPop_isa0:	db col_cmd, col_violet,"[POP HL]", col_cmd, col_white, "		[     MHz/      MHz]	in ISA1-RAM",cr,lf,0
msgPop_isa1:	db col_cmd, col_violet,"[POP HL]", col_cmd, col_white, "		[     MHz/      MHz]	in ISA2-RAM",cr,lf,0


msgLda:		db col_cmd, col_violet,"[LD A,(HL)]", col_cmd, col_white, "		[     MHz/      MHz]	in Main-RAM",cr,lf
		db col_cmd, col_violet,"[LD A,(HL)]", col_cmd, col_white, "		[     MHz/      MHz]	in Fast-RAM",cr,lf,0
msgLda_isa0:	db col_cmd, col_violet,"[LD A,(HL)]", col_cmd, col_white, "		[     MHz/      MHz]	in ISA1-RAM",cr,lf,0
msgLda_isa1:	db col_cmd, col_violet,"[LD A,(HL)]", col_cmd, col_white, "		[     MHz/      MHz]	in ISA2-RAM",cr,lf,0


msgLdhl:	db col_cmd, col_violet,"[LD (HL),A]", col_cmd, col_white, "		[     MHz/      MHz]	in Main-RAM",cr,lf
		db col_cmd, col_violet,"[LD (HL),A]", col_cmd, col_white, "		[     MHz/      MHz]	in Fast-RAM",cr,lf,0
msgLdhl_isa0:	db col_cmd, col_violet,"[LD (HL),A]", col_cmd, col_white, "		[     MHz/      MHz]	in ISA1-RAM",cr,lf,0
msgLdhl_isa1:	db col_cmd, col_violet,"[LD (HL),A]", col_cmd, col_white, "		[     MHz/      MHz]	in ISA2-RAM",cr,lf,0


msgResult:	db col_cmd, col_brown, "%u.%u",cr,lf,0
;msgPUSHrr_res:	db col_cmd, col_brown, "%u.%u",cr,lf,0



		DISPLAY "programm size: ", $-EXEHeader
Loader_End:
		DISPLAY "last address: ", $
		savebin "timerv2.exe",EXEHeader,$-EXEHeader
