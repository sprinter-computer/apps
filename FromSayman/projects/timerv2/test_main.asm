;-------------------------------------------------------------------------------
;INC/DEC HL
;-------------------------------------------------------------------------------
maintest0:	call turbo_off			;отключить турбу
		call CMOS_TEST			;цикл теста
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		ld d,3
		ld e,17
		ld a,d
		push af
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турбо уже включено, повторно не обязательно включать
		call CMOS_TEST
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		ld d,3
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;in fast-ram
		di
		call turbo_off
		call fastram_on
		call _framCMOS_TEST
		ex af,af
		call turbo_on
		call fastram_off
		ex af,af
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		ld e,17
		pop af
		inc a
		push af
		ld d,a
		call GoToXY
		ld de,result_tst_std
		call cPrint

		di
		call fastram_on
		call _framCMOS_TEST
		ex af,af
		call fastram_off
		ex af,af
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green
		ld (msgResult+1),a
		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a
;inc/dec
;isa0
		ld a,(use_isa0)
		or a
		jp z,.next_tst0
		di
		call turbo_off
		ld de,(ISA.SLOT0_RAM*256)+0x37
		call isa_on
		call _iramCMOS_TEST
		ex af,af
		call turbo_on
		ex af,af
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

		di
		call _iramCMOS_TEST
		ex af,af
		call isa_off
		ex af,af
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green
		ld (msgResult+1),a
		ld de,result_tst_trb
		call cPrint

;inc/dec isa1
.next_tst0:	ld a,(use_isa1)
		or a
		jp z,.next_tst1
		di
		call turbo_off
		ld de,(ISA.SLOT1_RAM*256)+0x37
		call isa_on
		call _iramCMOS_TEST
		ex af,af
		call turbo_on
		ex af,af
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

		di
		call _iramCMOS_TEST
		ex af,af
		call isa_off
		ex af,af
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green
		ld (msgResult+1),a
		ld de,result_tst_trb
		call cPrint

;-------------------------------------------------------------------------------
;PUSH HL
;-------------------------------------------------------------------------------
;теперь тест PUSH rr (в данном случае Push HL)
.next_tst1:	ld a,col_brown			;вернули цвет для 3.5мгц
		ld (msgResult+1),a
		ld hl,pushrr_tst
		ld (CMOS_TEST.ptr),hl		;сменили указатель на п/п теста

		call turbo_off			;отключить турбу
		call CMOS_TEST			;цикл теста
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a
		
		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;PUSH rr теперь тест в турбе, турба уже включена
		call CMOS_TEST
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;*** PUSH rr
;тепер тоже самое, но в fast-ram`е
		ld a,col_brown			;вернули цвет для 3.5мгц
		ld (msgResult+1),a
		ld hl,_fram_pushrr_tst
		ld (_framCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call fastram_on
		call turbo_off			;отключить турбу
		ld (sav_sp),sp
		ld sp,0x3fff
		call _framCMOS_TEST		;цикл теста
		ld sp,(sav_sp)
		call turbo_on			;включить турбу
		call fastram_off
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена PUSH rr
		di
		call fastram_on
		ld (sav_sp),sp
		ld sp,0x3fff
		call _framCMOS_TEST
		ld sp,(sav_sp)
		call fastram_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;*** PUSH rr
;isa-ram0
		ld a,(use_isa0)
		or a
		jp z,.next_tst2
		ld de,(ISA.SLOT0_RAM*256)+0x37
		call isa_on
		ld hl,_iram_pushrr_tst
		ld (_iramCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call turbo_off			;отключить турбу
		ld (sav_sp),sp
		ld sp,0xcfff
		call _iramCMOS_TEST		;цикл теста
		ld sp,(sav_sp)
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена PUSH rr
		di
		ld (sav_sp),sp
		ld sp,0xcfff
		call _iramCMOS_TEST
		ld sp,(sav_sp)
		call isa_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a
;*** PUSH rr
;isa-ram1
.next_tst2:	ld a,(use_isa1)
		or a
		jp z,.next_tst3
		ld de,(ISA.SLOT1_RAM*256)+0x37
		call isa_on
		ld hl,_iram_pushrr_tst
		ld (_iramCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call turbo_off			;отключить турбу
		ld (sav_sp),sp
		ld sp,0xcfff
		call _iramCMOS_TEST		;цикл теста
		ld sp,(sav_sp)
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена PUSH rr
		di
		ld (sav_sp),sp
		ld sp,0xcfff
		call _iramCMOS_TEST
		ld sp,(sav_sp)
		call isa_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a
;-------------------------------------------------------------------------------
;POP HL
;-------------------------------------------------------------------------------
;теперь тест POP rr (в данном случае POP HL)
.next_tst3:	ld hl,poprr_tst
		ld (CMOS_TEST.ptr),hl		;сменили указатель на п/п теста

		call turbo_off			;отключить турбу
		call CMOS_TEST			;цикл теста
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;POP rr теперь тест в турбе, турба уже включена
		call CMOS_TEST
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;*** POP rr
;тепер тоже самое, но в fast-ram`е
		ld hl,_fram_poprr_tst
		ld (_framCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call fastram_on
		call turbo_off			;отключить турбу
		ld (sav_sp),sp
		ld sp,_framCMOS_TEST+_fast_ram_test_size+10
		call _framCMOS_TEST		;цикл теста
		ld sp,(sav_sp)
		call turbo_on			;включить турбу
		call fastram_off
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена POP rr
		di
		call fastram_on
		ld (sav_sp),sp
		ld sp,_framCMOS_TEST+_fast_ram_test_size+10
		call _framCMOS_TEST
		ld sp,(sav_sp)
		call fastram_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;** POP rr
;isa0
		ld a,(use_isa0)
		or a
		jp z,.next_tst4
		ld de,(ISA.SLOT0_RAM*256)+0x37
		call isa_on

		ld hl,_iram_poprr_tst
		ld (_iramCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call turbo_off			;отключить турбу
		ld (sav_sp),sp
		ld sp,_iramCMOS_TEST+_isa_ram_test_size+10
		call _iramCMOS_TEST		;цикл теста
		ld sp,(sav_sp)
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена POP rr
		di
		ld (sav_sp),sp
		ld sp,_iramCMOS_TEST+_isa_ram_test_size+10
		call _iramCMOS_TEST
		ld sp,(sav_sp)
		call isa_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;pop rr
;isa1
.next_tst4:	ld a,(use_isa1)
		or a
		jp z,.next_tst5
		ld de,(ISA.SLOT1_RAM*256)+0x37
		call isa_on

		ld hl,_iram_poprr_tst
		ld (_iramCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call turbo_off			;отключить турбу
		ld (sav_sp),sp
		ld sp,_iramCMOS_TEST+_isa_ram_test_size+10
		call _iramCMOS_TEST		;цикл теста
		ld sp,(sav_sp)
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена POP rr
		di
		ld (sav_sp),sp
		ld sp,_iramCMOS_TEST+_isa_ram_test_size+10
		call _iramCMOS_TEST
		ld sp,(sav_sp)
		call isa_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a
;-------------------------------------------------------------------------------
;LD A,(HL)
;-------------------------------------------------------------------------------
;теперь тест LD A,(HL)
.next_tst5:	ld a,col_brown			;вернули цвет для 3.5мгц
		ld (msgResult+1),a
		ld hl,lda_hl_tst
		ld (CMOS_TEST.ptr),hl		;сменили указатель на п/п теста

		call turbo_off			;отключить турбу
		call CMOS_TEST			;цикл теста
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;LD A,(HL) еперь тест в турбе, турба уже включена
		call CMOS_TEST
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

;		ld d,9
		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;тепер тоже самое, но в fast-ram`е
		ld a,col_brown			;вернули цвет для 3.5мгц
		ld (msgResult+1),a
		ld hl,_fram_lda_hl_tst
		ld (_framCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call fastram_on
		call turbo_off			;отключить турбу
		call _framCMOS_TEST		;цикл теста
		call turbo_on			;включить турбу
		call fastram_off
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена
		di
		call fastram_on
		call _framCMOS_TEST
		call fastram_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;*** LD A,(HL)
;isa0
		ld a,(use_isa0)
		or a
		jp z,.next_tst6
		ld de,(ISA.SLOT0_RAM*256)+0x37
		call isa_on

		ld hl,_iram_lda_hl_tst
		ld (_iramCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call turbo_off			;отключить турбу
		call _iramCMOS_TEST		;цикл теста
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена
		di
		call _framCMOS_TEST
		call isa_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a
;isa1
.next_tst6:	ld a,(use_isa1)
		or a
		jp z,.next_tst7
		ld de,(ISA.SLOT1_RAM*256)+0x37
		call isa_on

		ld hl,_iram_lda_hl_tst
		ld (_iramCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call turbo_off			;отключить турбу
		call _iramCMOS_TEST		;цикл теста
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена
		di
		call _iramCMOS_TEST
		call isa_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;-------------------------------------------------------------------------------
;LD (HL),A
;-------------------------------------------------------------------------------
;теперь тест LD (HL),A
.next_tst7:	ld hl,ldhl_a_tst
		ld (CMOS_TEST.ptr),hl		;сменили указатель на п/п теста

		call turbo_off			;отключить турбу
		call CMOS_TEST			;цикл теста
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;LD (HL),A еперь тест в турбе, турба уже включена
		call CMOS_TEST
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;тепер тоже самое, но в fast-ram`е
		ld a,col_brown			;вернули цвет для 3.5мгц
		ld (msgResult+1),a
		ld hl,_fram_ldhl_a_tst
		ld (_framCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call fastram_on
		call turbo_off			;отключить турбу
		call _framCMOS_TEST		;цикл теста
		call turbo_on			;включить турбу
		call fastram_off
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a

		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена
		di
		call fastram_on
		call _framCMOS_TEST
		call fastram_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a
;*** LD (HL),a
;isa0
		ld a,(use_isa0)
		or a
		jp z,.next_tst8
		ld de,(ISA.SLOT0_RAM*256)+0x37
		call isa_on

		ld hl,_iram_ldhl_a_tst
		ld (_iramCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call turbo_off			;отключить турбу
		call _iramCMOS_TEST		;цикл теста
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена
		di
		call _iramCMOS_TEST
		call isa_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

;ld (hl),a
;isa1
.next_tst8:	ld a,(use_isa1)
		or a
		jp z,.next_tst9
		ld de,(ISA.SLOT1_RAM*256)+0x37
		call isa_on

		ld hl,_iram_ldhl_a_tst
		ld (_iramCMOS_TEST.ptr),hl	;сменили указатель на п/п теста

		di
		call turbo_off			;отключить турбу
		call _iramCMOS_TEST		;цикл теста
		call turbo_on			;включить турбу
		ret c				;ошибка тестирования, выход

		ld c,100
		call HL_Div_C
		ld (result_tst_std.KHz),a
		ld a,l
		ld (result_tst_std.MHz),a

		pop af
		inc a
		push af
		ld d,a
		ld e,17
		call GoToXY
		ld de,result_tst_std
		call cPrint

;турба уже включена
		di
		call _iramCMOS_TEST
		call isa_off
		ret c

		ld c,100
		call HL_Div_C
		ld (result_tst_trb.KHz),a
		ld a,l
		ld (result_tst_trb.MHz),a

		pop af
		push af
		ld d,a
		ld e,26
		call GoToXY
		ld a,col_green			;поменяли цвет мегагерц
		ld (msgResult+1),a

		ld de,result_tst_trb
		call cPrint
		ld a,col_brown			;вернули обрано цвет для 3.5мгц
		ld (msgResult+1),a

.next_tst9:	pop af				;restore stack at exit
		call turbo_on			;на всякий случай
		ret
