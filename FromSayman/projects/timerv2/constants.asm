;Color settings
col_blue = 1
col_green = 2
col_violet = 3
col_red = 4
col_magenta = 5
col_brown = 6
col_white = 7

col_yellow = 10
col_white_light = 15

col_cmd = 16

CMOS_DRD equ	0FFBDh
CMOS_DWR equ	0BFBDh
CMOS_AWR equ	0DFBDh

