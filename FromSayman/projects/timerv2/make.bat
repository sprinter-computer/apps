@echo off
if EXIST timerv2.exe (
	del timerv2.exe
)
if EXIST timerv2.lst (
	del timerv2.lst
)

@rem ..\..\asm\build.exe
..\..\asm\sjasmplus.exe timerv2.asm --lst=timerv2.lst
if errorlevel 1 goto ERR
echo Ok!
goto END

:ERR
del timerv2.exe
del timerv2.lst
pause
echo ERROR...
goto END

:END
