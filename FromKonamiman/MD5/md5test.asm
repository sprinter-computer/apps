; MD5TEST.ASM by Shaos 29-MAY-2021

        org   #3F00

;EXE-file header:
        dw 5845h     ; EXE Signature
        db 45h       ; Reserved (EXE type)
        db 00h       ; Version of EXE file
        dw 0200h     ; Code offset
        dw 0000h
        dw 0000h     ; Primary loader size or 0 (no primary loader)
        dw 0000h     ; Reserved
        dw 0000h     ; Reserved
        dw 0000h     ; Reserved
        dw START     ; Loading address
        dw START     ; Starting address (register PC)
        dw ENDSP     ; Stack address (register SP)
        ds 490       ; Reserved

PAGE0	EQU	082h
PAGE1	EQU	0A2h
PAGE2	EQU	0C2h
PAGE3	EQU	0E2h
PORT_Y	EQU	089h

;code of program

START:

	ld	a,0C0h
	out	(PORT_Y),a

; Write welcome message

	ld	hl,msg
	ld	c,5Ch
	rst	10h

	ld	bc,023Dh ; 2 pages
	rst	10h              
	push	af
	ld	bc,013Ah ; index 1 to page 2
	rst	10h
	pop	af
	ld	bc,003Bh ; index 3 to page 3
	rst	10h

; Print pages identifiers for every window

	in	a,(PAGE0)
	call	print_hex
	call	print_space

	in	a,(PAGE1)
	call	print_hex
	call	print_space

	in	a,(PAGE2)
	call	print_hex
	call	print_space

	in	a,(PAGE3)
	call	print_hex
	call	print_newline

; Wait keypress

;       ld   c,30h
;       rst   10h
;	call	print_hex
;	call	print_newline

	ld	hl,data
	ld	bc,0
count1:	ld	a,(hl)
	or	a
	jr	z,count0
	inc	bc
	inc	hl
	jr	count1
count0: ld	hl,data
	call	debug
	call	md5lib
	call	debug
	ld	c,16
loop1:	ld	a,(hl)
	push	bc
	push	hl
	call	print_hex
	pop	hl
	pop	bc
	inc	hl
	dec	c
	jr	nz,loop1
	call	print_newline

; Exit program

	ld	bc,0041h
	rst	10h

print_hex:
	push	af
	rra
	rra
	rra
	rra
	and	15
	cp	10
	jr	nc,L1
	add	a,'0'
	jr	L2
L1	add	a,'A'-10
L2	ld	c,5Bh
	rst	10h
	pop	af
	and	15
	cp	10
	jr	nc,L3
	add	a,'0'
	jr	L4
L3	add	a,'A'-10
L4	ld	c,5Bh
	rst	10h
	ret

print_space:
	ld	a,32 
print_char:
	ld	c,5Bh
	rst	10h
	ret

print_newline:
	ld	a,13
	ld	c,5Bh
	rst	10h
	ld	a,10
	ld	c,5Bh
	rst	10h
	ret

debug:	pop	ix
	push	ix
	push	af
	push	bc
	push	de
	push	hl
	push	iy
	push	ix
	ld	hl,msgpc
	ld	c,5Ch
	rst	10h
	pop	bc
	push	bc
	ld	a,b
	call	print_hex
	pop	bc
	ld	a,c
	call	print_hex
	ld	hl,0
	add	hl,sp
	push	hl
	pop	ix
	ld	hl,msgiy
	ld	c,5Ch
	rst	10h
	ld	a,(IX+1)
	call	print_hex
	ld	a,(IX+0)
	call	print_hex
	ld	hl,msghl
	ld	c,5Ch
	rst	10h
	ld	a,(IX+3)
	call	print_hex
	ld	a,(IX+2)
	call	print_hex
	ld	hl,msgde
	ld	c,5Ch
	rst	10h
	ld	a,(IX+5)
	call	print_hex
	ld	a,(IX+4)
	call	print_hex
	ld	hl,msgbc
	ld	c,5Ch
	rst	10h
	ld	a,(IX+7)
	call	print_hex
	ld	a,(IX+6)
	call	print_hex
	ld	hl,msgaf
	ld	c,5Ch
	rst	10h
	ld	a,(IX+9)
	call	print_hex
	ld	a,(IX+8)
	call	print_hex
	call	print_newline

	jr	debug0

	ld	c,30h
	rst	10h
	cp	#1B
        jr	nz,debug1
	ld	c,41h
	rst	10h
debug0:	call	print_newline
debug1:	pop	iy
	pop	hl
	pop	de
	pop	bc
	pop	af
	ret

msgpc	db	" PC=",0
msgiy	db	" IY=",0
msghl	db	" HL=",0
msgde	db	" DE=",0
msgbc	db	" BC=",0
msgaf	db	" AF=",0
msg	db	"MD5TEST",13,10,0

md5lib:
	include "md5.asm"

;Some values for testing (taken from RFC1321):
;MD5 ("") = d41d8cd98f00b204e9800998ecf8427e
;MD5 ("a") = 0cc175b9c0f1b6a831c399e269772661
;MD5 ("abc") = 900150983cd24fb0d6963f7d28e17f72
;MD5 ("message digest") = f96b697d7cb7938d525a2f31aaf161d0
;MD5 ("abcdefghijklmnopqrstuvwxyz") = c3fcd3d76192e4007dfb496cca67e13b
;MD5 ("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789") = d174ab98d277d9f5a5611c2c9f419d9f
;MD5 ("12345678901234567890123456789012345678901234567890123456789012345678901234567890") = 57edf4a22be3c955ac49da2e2107b67a

;data	db	0
;data	db	"a",0
;data	db	"abc",0
;data	db	"message digest",0
;data	db	"abcdefghijklmnopqrstuvwxyz",0
;data	db	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",0
data	db	"12345678901234567890123456789012345678901234567890123456789012345678901234567890",0

	ds	256

ENDSP	db	0
